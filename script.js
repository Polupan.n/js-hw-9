"use strickt";

// Теоретичні питання
// 1. Опишіть своїми словами що таке Document Object Model (DOM)

// DOM - це об`єктна модель документа. Структуру DOM представляє дерево об`єктів. Кожен елемент HTML стає об'єктом у цій моделі, що дозволяє за допомогою скриптів змінювати структуру, властивості та вміст сторінки в реальному часі.

// 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

// innerHTML:
// innerHTML повертає або встановлює HTML-код всередині елемента. Він включає в себе всі теги HTML, які містяться всередині елемента.
// Коли встановлюється значення innerHTML, браузер переробляє його в HTML. Ми можемо вставляти HTML-розмітку та створювати вкладені елементи.
// Використання innerHTML може бути потенційно небезпечним, особливо якщо дані отримані від користувача або не перевірені.

// innerText:
// innerText повертає або встановлює тільки текстовий вміст елемента, без HTML-тегів. Він ігнорує всі HTML-теги всередині елемента.
// Коли ми встановлюємо значення innerText, будь-які HTML-теги, які ми вставляємо, розглядаються як звичайний текст і вони відображаються як рядковий текст.
// innerText зазвичай використовується, коли потрібно працювати з текстом без урахування HTML-розмітки, наприклад, для отримання або встановлення тексту користувачем.

// 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
// До елемента сторінки можна звернутися за допомогою JS:
// За ідентифікатором (id):
let element = document.getElementById("elementId");
// За класом (class):
let elementsByClass = document.getElementsByClassName("className");
let elementsByQuery = document.querySelectorAll(".className");
// За тегом (tag):
let elementsByTag = document.getElementsByTagName("tagname");
// За селектором CSS:
let elementBySelector = document.querySelector("#elementId");
let elementsBySelector = document.querySelectorAll(".className");

// У кожного методу є свої переваги та обмеження, тому важливо враховувати контекст завдання, та потреби проекту при виборі найкращого способу доступу до елементів сторінки.

// 4. Яка різниця між nodeList та HTMLCollection?

// Основна різниця між NodeList і HTMLCollection полягає у:
// NodeList: Це об'єкт, який представляє собою колекцію вузлів документа. Вузли можуть бути елементами, текстомові вузли, коментарі або інші типи вузлів.
// `HTMLCollection`: Це спеціалізована версія `NodeList`, яка містить тільки елементи HTML. Вона зазвичай повертається методами, такими як `getElementsByTagName`, `getElementsByClassName` та `querySelectorAll`.

// `NodeList`: Може містити будь-які вузли документа.
// `HTMLCollection`: Містить тільки елементи HTML.

// `NodeList`: Може бути динамічним або статичним. Динамічний NodeList автоматично оновлюється при зміні вмісту документа, статичний залишається незмінним після створення.
// `HTMLCollection`: Зазвичай є динамічним, автоматично оновлюється при зміні вмісту документа.

// `NodeList`: Має методи, що дозволяють ітерувати через елементи і отримувати їх за індексом.
// `HTMLCollection`: Має ті ж самі методи, що й NodeList, а також методи, які специфічні для роботи з елементами HTML.

// Практичні завдання
//  1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
//  Використайте 2 способи для пошуку елементів.
//  Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).

const featureElements1 = document.querySelectorAll(".feature");
console.log(featureElements1);
const featureElements2 = document.getElementsByClassName("feature");
console.log(featureElements2);
featureElements1.forEach((element) => (element.style.textAlign = `center`));

//  2. Змініть текст усіх елементів h2 на "Awesome feature".
const titleH2 = document.getElementsByTagName(`h2`);
const titleArray = [...titleH2];
titleArray.forEach((element) => {
  element.textContent = "Awesome feature";
});
console.log(titleH2);

//  3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".
const elemByClass = document.querySelectorAll(".feature-title");
elemByClass.forEach((element) => (element.textContent += `!`));
console.log(elemByClass);
